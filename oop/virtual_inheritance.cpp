#include <iostream>
#include <string>

using namespace std;

// ------------------------------------- Class A ---------------------------------
class A {
    private:
        int m1;
    protected:
        int m2;
        void f1(string s) {
            cout << "@A::f1(" << s << ")" << endl;
        }
        void f2(string s);
    public:
        int m3;
        
        A(int a = 1, int b = 2, int c = 3): m1(a), m2(b), m3(c) {
            cout << "@A():    m1 = "<< m1 << ", m2 = " << m2 << ", m3 = " << m3 <<  endl;
        }
        void f3(string s = "ABC");
};

void A::f2(string s = "UNK") {
    cout << "@A::f2(" << s << ")    "
         << "m1 = " << m1 << ", m2 = " << m2 << ", m3 = " << m3 << endl;
}

void A::f3(string s) {
    cout << "@A::f3(" << s << ")" << endl;
    cout << "Calling protected f1()..." << endl;
    f1("A::f3");
    cout << "Calling protected f2()..." << endl;
    f2("A::f3");
}


// --------------------------------------- Class B -----------------------------
// class B: public A {      // <--- This will create ERROR because of 'D'
class B: virtual public A {
    private:
        int m4;
    public:
        B(int a = 11, int b = 12, int c = 13, int d = 14): A(a, b, c) {
            m4 = d;
            cout << "@B():    m4 = " << m4 << endl;
        }
};

// -------------------------------------- Class C ------------------------------
// class C: public A {      // <--- This will create ERROR because of 'D'
class C: virtual public A {
    private:
        int m5;
    public:
        C(int a = 21, int b = 22, int c = 23, int e = 25): A(a, b, c) {
            m5 = e;
            cout << "@C():    m5 = " << m5 << endl;
        }
};

// ------------------------------------- Class D -------------------------------
class D: public B, public C {
    private:
        int m6;
    public:
        D(int a = 101, int b = 102, int c = 103, 
            int d = 104, 
            int e = 105,
            int f = 106):A(a, b, c), B(201, 202, 203, d), C(301, 302, 303, e) {
            m6 = f;
            cout << "@D():    m6 = " << m6 << endl;
        }
};


// main
int main(int argc, char *argv[]) {

    cout << "Creating object of A" << endl;
    A obj1; obj1.f3();
    cout << endl << endl;

    cout << "Creating object of B" << endl;
    B obj2; obj2.f3();
    cout << endl << endl;

    cout << "Creating object of C" << endl;
    C obj3; obj3.f3("obj3");
    cout << endl << endl;

    cout << "Creating object of D" << endl;
    /*
        If not virtual inheritance, this will result in
            "error: request for member ‘f3’ is ambiguous"
    */
    D obj4; obj4.f3("obj4");
    /*
        Also the values of members of A will be the one given in
        its constrctor, not from B or C
    */
    cout << endl << endl;

    return 0;
}