/*
*   Generic Skip List using template
*/

#ifndef SKIP_LIST
#define SKIP_LIST

#include<iostream>
#include<iomanip>
#include<cstdlib>

const int maxLevel = 4;

template<class T>
class SkipListNode{
public:
    // Constructor
    SkipListNode() {
        
    }

    // members
    T key;
    SkipListNode **next;

    friend std::ostream& operator<<(std::ostream &out, const SkipListNode &node) {
        out << std::setfill(' ')
            << std::setw(12) << &node
            << std::setw(12) << node.key;
    }
};

// To deal with linker errors on fried function: https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
template<class T> class SkipList;
template<class T> std::ostream& operator<<(std::ostream &out, const SkipList<T> &list);

template<class T>
class SkipList{

private:
    typedef SkipListNode<T>* nodePtr;
    nodePtr root[maxLevel];
    int powers[maxLevel];

public:
    // constructors
    SkipList();

    // member functions
    bool isEmpty() const;
    void choosePowers();
    int chooseLevel();
    T* skipListSearch(const T&);
    void skipListInsert(const T&);

    // for out
    friend std::ostream& operator<< <>(std::ostream&, const SkipList&);
};

template<class T>
SkipList<T>::SkipList() {
    // create null pointers in the root
    for(int i = 0; i < maxLevel; i++) {
        root[i] = 0;
    }
    // choose powers
    choosePowers();
}

template<class T>
bool SkipList<T>::isEmpty() const {
    return root[0] == 0;
}

template<class T>
void SkipList<T>::choosePowers() {
    powers[maxLevel-1] = (2 << (maxLevel-1)) - 1;   // 2^maxLevel - 1
    for(int i = maxLevel - 2, j = 0; i >= 0; i--, j++) {
        powers[i] = powers[i+1] - (2 << j);
    }
}

template<class T>
int SkipList<T>::chooseLevel() {
    int i, r = rand() % powers[maxLevel-1] + 1;
    for(i = 1; i < maxLevel; i++) {
        if(r < powers[i])
            return i - 1;
    }
    return i - 1;
}

template<class T>
T* SkipList<T>::skipListSearch(const T &key) {
    if(isEmpty())
        return 0;
    
    nodePtr curr, prev;
    int lvl;
    //find the highest occupied level in the list
    for(lvl = maxLevel - 1; lvl >= 0 && !root[lvl]; lvl--);

    prev = curr = root[lvl];
    while(true) {
        if(key == curr->key) {
            return &curr->key;
        } else if(key < curr->key) {
            if(lvl == 0) { // already at lowest level
                return 0;
            } else if(curr == root[lvl]) {
                curr = root[--lvl];
            } else{
                curr = *(prev->next + --lvl);
            }
        } else {
            prev = curr;
            if(*(curr->next + lvl) != 0) {
                curr = *(curr->next + lvl);
            } else {
                for(lvl--; lvl >= 0 && *(curr->next + lvl) == 0; lvl--);
                if(lvl >= 0) {
                    curr = *(curr->next + lvl);
                } else {
                    return 0;
                }
            }
        }
    }
}

template<class T>
void SkipList<T>::skipListInsert(const T &key) {
    nodePtr curr[maxLevel], prev[maxLevel], newNode;
    int lvl, i;

    curr[maxLevel-1] = root[maxLevel-1];
    prev[maxLevel-1] = 0;

    std::cout << "    Inserting " << key << "\n";
    for(int i = 0; i < maxLevel; i++) {
        std::cout << root[i] << "    ";
    }
    std::cout << std::endl;

    for(lvl = maxLevel - 1; lvl >= 0; lvl--) {
        while(curr[lvl] && curr[lvl]->key < key) {
            prev[lvl] = curr[lvl];
            curr[lvl] = *(curr[lvl]->next + lvl);
        }
        if(curr[lvl] && curr[lvl]->key == key) {
            // don't include duplicates
            return;
        }
        if(lvl > 0) {
            if(prev[lvl] == 0) {
                curr[lvl-1] = root[lvl-1];
                prev[lvl-1] = 0;
            } else {
                curr[lvl-1] = *(prev[lvl]->next + lvl - 1);
                prev[lvl-1] = prev[lvl];
            }
        }
    }

    lvl = chooseLevel();
    std::cout << "Insert at level = " << lvl << std::endl;
    newNode = new SkipListNode<T>;
    newNode->next = new nodePtr[sizeof(nodePtr) * (lvl+1)];
    newNode->key = key;

    for(i = 0; i <= lvl; i++) {
        *(newNode->next + i) = curr[i];     // link next nodes
        if(prev[i] == 0) {                  // link previous nodes
            root[i] = newNode;
        } else {
            *(prev[i]->next + i) = newNode;
        }
    }
}


template<class T>
std::ostream& operator<<(std::ostream &out, const SkipList<T> &list) {
    out << "Skip List Info:\n"
        << "root: ";
    for(int i = 0; i < maxLevel; i++)
        out << std::setfill(' ') << std::setw(12) << list.root[i];
}

#endif