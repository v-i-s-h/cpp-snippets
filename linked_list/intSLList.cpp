#include <iostream>
#include <iomanip>

#include "intSLList.hpp"

IntSLList::~IntSLList() {
    for(IntSLLNode *p; !isEmpty(); ) {
        p = head->next;
        delete head;
        head = p;
    }
}

void IntSLList::addToHead(int data) {
    head = new IntSLLNode(data, head);
    if(tail == 0) {
        tail = head;
    }
}

void IntSLList::addToTail(int data) {
    if(tail != 0) {
        tail->next = new IntSLLNode(data);
        tail = tail->next;
    } else {
        head = tail = new IntSLLNode(data);
    }
}

int IntSLList::deleteFromHead() {
    int el = head->info;
    IntSLLNode *tmp = head;
    if(head == tail) {
        head = tail = 0;
    } else{
        head = head->next;
    }
    delete tmp;

    return el;
}

int IntSLList::deleteFromTail() {
    int el = tail->info;
    IntSLLNode *tmp = tail;
    if(head == tail) {
        head = tail = 0;
    } else {
        IntSLLNode *newtail;
        for(newtail = head; newtail->next != tail; newtail = newtail->next);
        tail = newtail;
        tail->next = 0;
    }
    delete tmp;

    return el;
}

void IntSLList::deleteNode(int data) {
    if(head != 0) { // non-empty list
        if(head == tail) {  // only one element
            if(head->info == data) {    // .. and a match!
                delete head;
                head = tail = 0;
            }
        } else {    // more than one element
            if(head->info == data) { // if the match is at head
                IntSLLNode *tmp = head;
                head = head->next;
                delete tmp;
            } else{
                // is it some where in the list?
                IntSLLNode *prev, *tmp;
                for(prev = head, tmp = head->next;
                    tmp != 0 && tmp->info != data;
                    prev = prev->next, tmp = tmp->next);
                if(tmp != 0) {
                    prev->next = tmp->next;
                    if(tmp == tail) {
                        tail = prev;
                    }
                    delete tmp;
                }

            }
        }
    }
}

bool IntSLList::IsInList(int data) const {
    bool result = false;

    for(IntSLLNode *node = head; node != 0; node = node->next) {
        if(node->info == data) {
            result = true;
            break;
        }
    }

    return result;
}

std::ostream& operator<<(std::ostream &out, const IntSLList &list) {
    out << "List Info:\n" 
        << "  Head: " << list.head << "    "
        << "  Tail: " << list.tail << "\n";
    out << "Data:\n";

    unsigned int i = 0;
    for(IntSLLNode *ptr = list.head; ptr != 0; i++, ptr = ptr->next){
        out << "    "
            << std::setfill('0') << std::setw(4) << i
            << "  " << ptr
            << std::setfill(' ') << std::setw(10) << ptr->info 
            // << "  " << ptr->next 
            << "\n";
    }

    return out;
}