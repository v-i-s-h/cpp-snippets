# Implementation of Linked Lists

## Singly Linked List
### Singly linked list with `int` member
Compile `g++ intSLList_test.cpp  intSLList.cpp -o test_sll.out`

Run `./test_sll.out`

#### Files
1. [`intSLList.hpp`](./intSLList.hpp)
2. [`intSLList.cpp`](./intSLList.cpp)
3. [`intSLList_test.cpp`](./intSLList_test.cpp) Test file for SLL.

## Doubly Linked List
### Generic doubly linked list with template
Compile `g++ dll_test.cpp DLList.cpp -o test_dll.out`

Run `./test_dll.out`

#### Files
1. [`DLList.hpp`](./DLList.hpp)
2. [`DLList.cpp`](./DLList.cpp)
3. [`dl_test.cpp`](./dl_test.cpp)

## Skip Lists
### Template Skip List
Compile`g++ skl_test.cpp skipList.cpp -o test_skl.out`

Run`./test_skl.out`

#### Files
1. [`skipList.hpp`](./skipList.hpp)
2. ['skipList.cpp`](./skipList.cpp)
3. [`skl_test.cpp`](./skl_test.cpp)