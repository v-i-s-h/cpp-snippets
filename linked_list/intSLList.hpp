/* 
* An implementation of singly linked lists for integers
*/

#ifndef INT_LINKED_LIST
#define INT_LINKED_LIST

#include <iostream>

class IntSLLNode {
public:
    
    // constructors
    IntSLLNode() {
        next = 0;
    }

    IntSLLNode(int data, IntSLLNode *ptr = 0) {
        info = data;
        next = ptr;
    }

    // data members
    int info;
    IntSLLNode *next;
};

class IntSLList {
public:
    IntSLList() {
        head = tail = 0;
    }
    ~IntSLList();

    // operations
    int isEmpty() {
        return head == 0;
    }

    void addToHead(int);
    void addToTail(int);
    int deleteFromHead();
    int deleteFromTail();
    void deleteNode(int);
    bool IsInList(int) const;

    // data members
    IntSLLNode *head, *tail;

    // Overload '<<' operator for printing with memory info
    friend std::ostream& operator<<(std::ostream &out, const IntSLList &list);
};

#endif