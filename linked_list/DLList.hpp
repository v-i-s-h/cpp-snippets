/*
* Implementation of Generic Doubly Linked List using Templates
*/

#ifndef DOUBLY_LINKED_LIST
#define DOUBLY_LINKED_LIST

#include <iostream>
#include <iomanip>

template<class T>
class DLLNode {
public:
    // Members
    T info;
    DLLNode *prev, *next;

    // Constructors
    DLLNode() {
        prev = 0;
        next = 0;
    }

    DLLNode(const T &el, DLLNode *n = 0, DLLNode *p = 0) {
        info = el;
        prev = p;
        next = n;
    }

    // template friend function definition -- inline. See below for another option
    friend std::ostream& operator<<(std::ostream &out, const DLLNode &node) {
        out << std::setfill(' ') 
        << std::setw(12) << &node
        << std::setw(12) << node.info
        << std::setw(12) << node.prev
        << std::setw(12) << node.next;
    }
};

// To deal with linker errors on fried function: https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
template<class T> class DoublyLinkedList;
template<class T> std::ostream& operator<<(std::ostream &out, const DoublyLinkedList<T> &list);

template<class T>
class DoublyLinkedList {
public:
    // Member
    DLLNode<T> *head, *tail;

    // Constructors
    DoublyLinkedList() {
        head = 0;
        tail = 0;
    }

    // Destructors
    ~DoublyLinkedList();
    
    // Member functions
    bool isEmpty() {
        return head == 0;
    }

    void addToHead(const T &el);
    void addToTail(const T &el);
    T deleteFromHead();
    T deleteFromTail();
    void deleteNode(T);
    bool IsInList(T) const;

    // overloading for printing -- definition is outside the class and hence
    // requires pre declaration (above this class) and special '<>'.
    friend std::ostream& operator<< <>(std::ostream&, const DoublyLinkedList&);
};

template<class T>
DoublyLinkedList<T>::~DoublyLinkedList() {
    for(DLLNode<T> *ptr; !isEmpty(); ) {
        ptr = head->next;
        delete head;
        head = ptr;
    }
}

template<class T>
void DoublyLinkedList<T>::addToHead(const T &el) {
    DLLNode<T> *newnode = new DLLNode<T>(el, head, 0);
    if(head != 0) {
        head->prev = newnode;
    } else {
        // this is the first node
        tail = newnode;
    }
    head = newnode;
}

template<class T>
void DoublyLinkedList<T>::addToTail(const T &el) {
    DLLNode<T> *newnode = new DLLNode<T>(el, 0, tail);
    if(tail != 0) {
        tail->next = newnode;
    } else {
        // this is the first node
        head = newnode;
    }
    tail = newnode;
}

template<class T>
T DoublyLinkedList<T>::deleteFromHead() {
    T el = head->info;
    DLLNode<T> *tmp = head;
    if(head == tail) {
        head = tail = 0;
    } else {
        head = head->next;
        head->prev = 0;
    }
    delete tmp;

    return el;
}

template<class T>
T DoublyLinkedList<T>::deleteFromTail() {
    T el = tail->info;
    DLLNode<T> *tmp = tail;
    if(head == tail) {
        head = tail = 0;
    } else {
        tail = tail->prev;
        tail->next = 0;
    }
    delete tmp;

    return el;
}

template<class T>
void DoublyLinkedList<T>::deleteNode(T data) {
    std::cout << "DELETE: " << data 
             << "    HEAD " << head
             << "    TAIL " << tail << std::endl;
    if(head == tail) {
        // only one node
        if(head->info == data) {
            delete head;
            head = tail = 0;
        } 
    } else {
        if(head->info == data) {
            DLLNode<T> *tmp = head;
            head = head->next;
            head->prev = 0;
            delete tmp;
        }
        else if(tail->info == data) {
            DLLNode<T> *tmp = tail;
            tail = tail->prev;
            tail->next = 0;
            delete tmp;
        } else {
            DLLNode<T> *node;
            for(node = head->next;
                node != 0 && node->info != data;
                node = node->next);
            if(node != 0) {
                node->prev->next = node->next;
                node->next->prev = node->prev;
                delete node;
            }
        }
    }
}

template<class T>
bool DoublyLinkedList<T>::IsInList(T data) const{
    bool result = false;
    for(DLLNode<T> *node = head; node != 0; node = node->next){
        if(node->info == data) {
            result = true;
        }
    }
    return result;
}

template<class T>
std::ostream& operator<<(std::ostream &out, const DoublyLinkedList<T> &list) {
    out << "List Info:\n" 
        << "  Head: " << list.head << "    "
        << "  Tail: " << list.tail << "\n";
    out << "Data:\n";

    unsigned int i = 0;
    for(DLLNode<T> *ptr = list.head; ptr != 0; i++, ptr = ptr->next){
        out << "    " << *ptr << "\n";
    }

    return out;
}

#endif