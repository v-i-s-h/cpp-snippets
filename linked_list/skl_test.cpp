/*
*   Compile : g++ skl_test.cpp -o skl_test.out
*   Run     : ./skl_test.out
*/

#include <iostream>
#include <iomanip>

#include "skipList.hpp"

using namespace std;

void test_int() {
    SkipList<int> list;
    cout << "Skip list created...\n"
         << list << "\n";
    
    cout << "Inserting elements...\n";
    for(int i = 0; i < 3; i++) {
        list.skipListInsert(i);
    }
    cout << list << std::endl;
}

void test_float() {

}

int main(int argc, char *argv[]) {

    test_int();

    return 0;
}