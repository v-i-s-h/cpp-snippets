/*
* Run: g++ intSLList_test.cpp  intSLList.cpp -o test_sll.out -std=c++11 && ./test_sll.out
*/
#include <iostream>
#include "intSLList.hpp"

using namespace std;

int main(int argc, char *argv[]) {

    IntSLList list;

    cout << "Initialized list...\n" << list << endl;
    // {
    //     // failure case
    //     cout << "Deleting from head...\n";
    //     int el = list.deleteFromHead();
    //     cout << "value = " << el << "\n";
    // }

    // add come elements
    for(int i = 0; i < 15; i++)
        list.addToHead(i);
    cout << "Data added...\n" << list << endl;

    // delete from head
    cout << "Deleting from head...\n";
    for(int i = 0; i < 3; i++) {
        int el = list.deleteFromHead();
        cout << el << ", ";
    }
    cout << "\n";
    cout << list << endl;

    // delete from tail
    cout << "Deleting from tail...\n";
    for(int i = 0; i < 3; i++) {
        int el = list.deleteFromTail();
        cout << el << ", ";
    }
    cout << "\n";
    cout << list << endl;

    // delete element
    cout << "deleting element 7...\n";
    list.deleteNode(7);
    cout << list << endl;

    // search 
    cout << "Is 7 in?    " << (list.IsInList(7) ? "YES" : " NO") << "\n";
    cout << "Is 8 in?    " << (list.IsInList(3) ? "YES" : " NO") << '\n';

    cout << "delete from head and tail...\n";
    list.deleteNode(11); list.deleteNode(10);
    list.deleteNode(3); list.deleteNode(4);
    cout << list << endl;

    return 0;
}