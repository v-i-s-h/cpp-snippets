/*
*   Compile : g++ dll_test.cpp DLList.cpp -o dll_test.out
*   Run     : ./dll_test.out
*/

#include <iostream>
#include <iomanip>

#include "DLList.hpp"

using namespace std;

void test_int() {
    DoublyLinkedList<int> list;
    
    cout << "Initialized list..." << list << "\n";
    
    // add some elements
    for(int i = 0; i < 15; i++)
        list.addToHead(i);
    cout << "Data added to head...\n" << list << endl;
    
    // add to tail
    for(int i = 1; i < 5; i++)
        list.addToTail(i);
    cout << "Data added to tail...\n" << list << endl;

    // delete from head
    cout << "Deleted elements: ";
    for(int i = 0; i < 6; i++) {
        int el = list.deleteFromHead();
        cout << el << ", ";
    }
    cout << "\nAfter deleting from head...\n" << list << endl;

    // delte from tail
    cout << "Deleted Elements: ";
    for(int i = 0; i < 3; i++) 
        cout << list.deleteFromTail() << ", ";
    cout << "\nAfter deleting from tail...\n" << list << endl;

    // check list
    cout << "Checking for elements...\n";
    for(int i = -3; i < 5; i++)
        cout << i << "    " << (list.IsInList(i)?"YES":" NO") << "\n";

    // delete
    cout << "Deleting element...\n";
    list.deleteNode(0);
    cout << "After deleting 0...\n" << list << "\n";
    list.deleteNode(1);
    cout << "After deleting 1...\n" << list << "\n";

    cout << "Deleting range -15 to 15...\n";
    for(int i = -15; i < 15; i++)
        list.deleteNode(i);
    cout << "After deleting...\n" << list << "\n"; // !SegFault : deleting from empty array
}

void test_float() {
    DoublyLinkedList<float> list2;
    cout << "Initialized list..." << list2 << "\n";
    
    for(float i = 0.0f; i < 2.5; i += 0.75)
        list2.addToTail(i);
    cout << "Data added to tail...\n" << list2 << "\n";

    for(float i = 0.75; i < 2.5; i += 0.75)
        list2.addToHead(i);
    cout << "Data added to head...\n" << list2 << "\n";
}

int main(int argc, char *argv[]) {

    test_int();
    // test_float();

    return 0;
}

