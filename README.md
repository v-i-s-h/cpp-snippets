# cpp-snippets

Collection of C++ snippets
## Contents
- [strings](./strings/README.md) : String manipulation related snippets
- [oop](./oop/README.md) : Collection of snippets which deals with C++ OOP concepts
- [linked_list](./linked_list/README.md) : Linked lists and associated algorithms
- [lambdas](./lambdas/README.md) : Examples of using Lambdas in C++