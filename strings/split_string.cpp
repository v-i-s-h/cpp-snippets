// Get input from commandline split the string based on delimiter.
// Source: https://www.hackerrank.com/challenges/simple-array-sum/problem

#include <iostream>     // For IO
#include <algorithm>    // For std::unique, std::distance
#include <vector>       // For std::vector

// Function to implement string splitting
std::vector<std::string> split_string(std::string);


// Utility functions
void display_str(std::string);

int main(int argc, char *argv[]) {

    // Get input
    std::string str;
    getline(std::cin, str);

    // Split string into vector of strings
    std::vector<std::string> list_str = split_string(str);

    // Printing using index
    std::cout << "Using indices..." << std::endl;
    for(std::vector<std::string>::size_type i = 0; i < list_str.size(); i++) {
        std::cout << i << " " << list_str[i] << std::endl;
    }

    // Printing using iterator
    std::cout << "Using Iterator..." << std::endl;
    for(std::vector<std::string>::iterator it = list_str.begin();
        it != list_str.end();
        ++it) {
        std::cout << *it << std::endl;
    }

    // Range
    std::cout << "Using Range..." << std::endl;
    for(auto const& str: list_str) {
        std::cout << str << std::endl;
    }

    return 0;
}

std::vector<std::string> split_string(std::string in_str) {
    std::vector<std::string> list_str;

    // Remove multiple whitespaces if any
    std::string::iterator new_end = unique(in_str.begin(), in_str.end(),
                                        [](const char &x, const char &y){
                                            return x == y && x == ' ';
                                        });
    in_str.erase(new_end, in_str.end());

    // Removing any trailing whitespaces
    while(in_str[in_str.length() - 1] == ' ') {
        in_str.pop_back();
    }

    char delimiter = ' ';
    size_t i = 0;
    size_t pos = in_str.find(delimiter);
    
    while(pos != std::string::npos) {
        list_str.push_back(in_str.substr(i, pos - i));
        i = pos + 1;
        pos = in_str.find(delimiter, i);
    }

    for(auto const &str: list_str) {
        std::cout << str << std::endl;
    }

    list_str.push_back(in_str.substr(i, std::min(pos, in_str.length() - i + 1)));

    return list_str;
}