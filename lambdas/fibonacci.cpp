/*
*   Using stateful lambdas to generate Fibonacci sequence.
*   Compile: g++ fibonacci.cpp -o fibonacci.out -std=c++11
*   Source: https://www.youtube.com/watch?v=_1X9D8Z5huA
*
*/

#include <iostream>
#include <memory>

int main() {

    // define lambda
    auto l = [i = 0, j = 1]() mutable {
        i = std::exchange(j, j+i);
        return i;
    };

    // generate first 10 (acutually 0 is missing)
    for(int i = 0; i < 10; i++) {
        std::cout << l() << "\n";
    }

    return 0;
}