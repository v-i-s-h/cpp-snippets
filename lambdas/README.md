# Example with lambdas in C++

1. [`fibonacci.cpp`](./fibonacci.cpp): Generates Fibonacci sequence using stateful lambdas.